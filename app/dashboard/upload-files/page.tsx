import UploadFilesForm from "../../../components/dashboard/upload-files-form";

export default async function Page() {
    return (
        <div className="rounded-lg p-5 bg-white">
            <UploadFilesForm/>
        </div>
    )
}
