import {RawDataModel} from "../../../models/raw-data.model";
import {RawDataGraph} from "../../../components/dashboard/raw-data-graph";
import moment from "moment";

async function getRawData(): Promise<RawDataModel[]> {
    const res = await fetch(`${process.env.apiUrl}/raw-data`)

    if (!res.ok) {
        throw new Error('Failed to fetch data');
    }

    return populateRawData(await res.json());
}

function populateRawData(rawDatas: RawDataModel[]) {
    return rawDatas.map(rawData => {
        return {
            resultTime: moment(rawData.resultTime).format('DD-MM-YYYY hh:mm'),
            'Availability': rawData.availDur
        }
    })
}

export default async function Page() {
    const rawDatas: RawDataModel[] = await getRawData();

    return (
        <div className="rounded-lg p-5 bg-white w-full h-full">
            <RawDataGraph data={rawDatas} />
        </div>
    )
}
