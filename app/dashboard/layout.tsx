import BasePageComponents from "../../components/base-page-components";

export default function DashboardLayout({children}: {children: React.ReactNode}) {
    return (
        <BasePageComponents>
            { children }
        </BasePageComponents>
    )
}
