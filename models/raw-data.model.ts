export class RawDataModel {
    id?: string;
    resultTime?: Date | string;
    enodebId?: string;
    cellId?: string;
    availDur?: number;
}
