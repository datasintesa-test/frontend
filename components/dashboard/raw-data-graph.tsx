'use client';

import {CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import {Typography} from "antd";

const { Title } = Typography;

export function RawDataGraph(props) {
    const data = props.data || null;

    const renderGraph = (
        <LineChart
            width="100%"
            height="100%"
            data={data}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="resultTime" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="Availability" stroke="#8884d8" />
        </LineChart>
    )

    return (
        <div className="flex flex-col items-center gap-y-1.5 w-full h-full">
            <div className="w-full text-center">
                <Title level={2}>Raw Data Line Chart</Title>
            </div>
            <div className="w-full h-full">
                <ResponsiveContainer width="100%" height="100%">
                    {data && renderGraph }
                </ResponsiveContainer>
            </div>
        </div>
    );
}
