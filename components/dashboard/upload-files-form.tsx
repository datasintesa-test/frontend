'use client'

import React, {useRef, useState} from "react";
import {Button, message, Space, theme, Typography, Upload} from "antd";
import {RcFile} from "antd/lib/upload";
import {UploadOutlined} from "@ant-design/icons";

const { Title } = Typography;

export default function UploadFilesForm () {
    const fileState = useRef({ reset: () => {}, fileList: [] });
    const [uploadFiles, setUploadFiles] = useState([]);
    const [uploading, setUploading] = useState(false);
    const [validUpload, setValidUpload] = useState(false);

    const updateFiles = (list, setState) => {
        let fileList;
        const _ = (list, setState) => {
            if (!fileList) {
                fileList = list;
                setState && setState(list);
            }
            return {
                fileList,
                reset() {
                    fileList = [];
                }
            };
        };
        return _(list, setState);
    };

    const handleUpload = () => {
        if (uploadFiles.length > 15) {
            message.warning('Max 15 files to be uploaded.');
            setValidUpload(false);
            return;
        }

        const formData = new FormData();
        uploadFiles.forEach((file) => {
            formData.append('files[]', file as RcFile);
        });
        setUploading(true);
        fetch(`${process.env.apiUrl}/raw-data/upload`, {
            method: 'POST',
            body: formData,
        })
            .then((res) => res.json())
            .then((res) => {
                if (res.statusCode != 200) {
                    message.error(res.message);
                    clearFile();
                    return;
                }

                let messages = 'Successfully upload files.';

                if (res.data?.insertedCount) messages += ' Added' + res.data.insertedCount;

                if (res.data?.updatedCount) messages += ', modified' + res.data.updatedCount;

                if (res.data?.deletedCount) messages += ', deleted' + res.data.updatedCount;

                message.success(messages)

                clearFile();
            })
            .catch((e) => {
                console.log(e, 'error');
                message.error('Error upload files.');
            })
            .finally(() => {
                setUploading(false);
            });
    };

    const beforeUpload =  (_, fileList) => {
        fileList.current = updateFiles(fileList, setUploadFiles);
        setValidUpload(true);
        return false;
    }

    const clearFile = () => {
        fileState.current.reset();
        setUploadFiles([]);
        setValidUpload(false);
    }

    return (
        <div className="w-full flex flex-col items-center gap-y-1.5">
            <div className="w-full text-center">
                <Title level={2}>Upload Files</Title>
            </div>
            <div className="w-1/2">
                <Upload.Dragger multiple={true} fileList={uploadFiles} beforeUpload={beforeUpload} showUploadList={{ showRemoveIcon: false }} maxCount={15} disabled={uploading}>
                    <p className="ant-upload-drag-icon">
                        <UploadOutlined />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">
                        Support for a single or bulk upload. Only <strong>(.gzip/.gz)</strong> File extension only allowed.<br />
                        (Max 15 files uploaded at once)
                    </p>
                </Upload.Dragger>
                <Space size={"middle"}>
                    <Button
                        type="primary"
                        onClick={handleUpload}
                        disabled={uploadFiles.length === 0 || !validUpload}
                        loading={uploading}
                        style={{ marginTop: 16 }}
                    >
                        {uploading ? 'Uploading' : 'Start Upload'}
                    </Button>
                    <Button
                        type="primary"
                        danger
                        onClick={clearFile}
                        disabled={uploadFiles.length === 0}
                        style={{ marginTop: 16 }}
                    >
                        Clear Files
                    </Button>
                </Space>
            </div>
        </div>
    )
}
